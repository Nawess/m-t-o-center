#include <WiFi.h>
#include <WiFiMulti.h>
#include <HTTPClient.h>
#include <DHT_U.h>
#include <DHT.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <ArduinoJson.h>



#define POPSCHOOL

//load the config.h file
#include "config.h"

//INIT BORDEL

HTTPClient http;//create HTTP client object
HTTPClient http_send;

WiFiMulti WiFiMulti;//create object WifiMulti for test cnx

//Config 18B20 T° sensor
// Data wire is plugged into port 4 on the Arduino
#define ONE_WIRE_BUS 4
// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);
// Pass our oneWire reference to Dallas Temperature. Create sensors object
DallasTemperature sensors(&oneWire);
float t = 0; //init T° variable

//Config DHT11 sensor
#define DHTPIN 23// Digital pin connected to the DHT sensor
#define DHTTYPE DHT11   // define sensor type : DHT 11
DHT sensor(DHTPIN, DHTTYPE);//Init sensor. Create object
float h = 0; //init humidity variable
//init global variable
byte ledPin = 25;//OK led pin n°
byte ledPin2 = 15;//error led pin n°
float tempMinMax[] = {25, 15}; //array for min and max temperature store : 0=min; 1=max
float tmin = 0; //tmin variable
float tmax = 0; //tmax variable

StaticJsonDocument<1500> forecast;

void read_sensors() {

  // put your main code here, to run repeatedly:
  //18B20 sensor
  sensors.requestTemperatures(); // Send the command to get temperatures
  t = sensors.getTempCByIndex(0); //put Celsius T° in variable for sensor 0
  Serial.print("Temperature for the device 1 (index 0) is: ");
  Serial.println(t);//print T°

  //Min and Max temperatures check and replace if necessary
  if (t < tempMinMax[0]) //Temp Min
  {
    tempMinMax[0] = t;
  }
  if (t > tempMinMax[1]) //Temp Max
  {
    tempMinMax[1] = t;
  }
  tmin = tempMinMax[0];
  tmax = tempMinMax[1];
  Serial.print("Mini T° is : ");
  Serial.println(tmin);//print T° min to serial
  Serial.print("Maxi T° is : ");
  Serial.println(tmax);//print T° max to serial


  //DHT11 sensor
  h = sensor.readHumidity(); //read humidity from DHT11
  Serial.print("Humidity from DHT11 sensor is : ");
  Serial.println(h);//print hygro
}

void send_data() {
  //http send & connect
  String host = "node.gardrinet.fr";// Mon hostname
  String url = "/sensor/g09/" + String(t) + "/" + String(h) + "/" + String(tmin) + "/" + String(tmax);//Willems node url
  http_send.begin("https://" + host + url);//connect to host + url in HTTP

  int backCode = http_send.GET(); //send request to host server and put answer code in variable
  Serial.print("[HTTP] Return Code = ");
  Serial.println(backCode);//print HTTP answer code
  //test return code from server
  if (backCode > 0) //if all is OK
  {
    if (backCode == HTTP_CODE_OK) //if error code is ok
    {
      String content = http_send.getString(); //put response in content variable
    }
    else
    {
      Serial.println("[HTTP] : Oooups... Something is wrong with server !!!");
    }
  }
  else//if error -1 (can't find server)
  {
    Serial.print("[HTTP] GET failed error : ");
    Serial.println(backCode);
    digitalWrite(ledPin2, 1); //put error led on
    digitalWrite(ledPin, 0); //put OK led off
  }
  delay(3000);//pause 30s
  //delay(300000);//pause 5mn for router health
  http_send.end();//close http cnx
}

void get_data() {

  //http send & connect
  //Host to Contact
  String host = "api.openweathermap.org";

  //Url to Get :
  String url = "/data/2.5/forecast?q=willems,fr&appid=" + String (APIKEY) + "&cnt=1"; //url call api from openweathermap
  http.begin("http://" + host + url);//connect to host + url in HTTP
  Serial.println("http://" + host + url);
  int backCode = http.GET(); //send request to host server and put answer code in variable

  Serial.print("[HTTP] Return Code = ");
  Serial.println(backCode);//print HTTP answer code
  //test return code from server
  if (backCode > 0) //if all is OK
  {
    if (backCode == HTTP_CODE_OK) //if error code is ok
    {

      String content = http.getString(); //put response in content variable
      Serial.println(content);
      //Let's try to parse it :)
      DeserializationError error = deserializeJson(forecast, content);
      //test parsing suceeds
      if (error) {
        Serial.print(F("deserializeJson() failed: "));
        Serial.println(error.c_str());
        return;
      }//END IF ERROR
      //Parsing was OK grabbing values.
      float Wspeed=forecast["list"][0]["wind"]["speed"];
      float Wdirection=forecast["list"][0]["wind"]["deg"];
      Serial.print("Wind speed (m/s)=");
      Serial.println(Wspeed);
      Serial.print("Wind direction (degrés)=");
      Serial.println(Wdirection);
    }
    else
    {
      Serial.println("[HTTP] : Oooups... Something is wrong with server !!!");
    }
  }
  else//if error -1 (can't find server)
  {
    Serial.print("[HTTP] GET failed error : ");
    Serial.println(backCode);
    digitalWrite(ledPin2, 1); //put error led on
    digitalWrite(ledPin, 0); //put OK led off
  }
  delay(3000);//pause 30s
  //delay(300000);//pause 5mn for router health
  http.end();//close http cnx
}

void setup() {
  Serial.begin(115200);
  // put your setup code here, to run once:
  // Connecting to The AP
  WiFiMulti.addAP(SSID, PWD);
  // While the wifi is note connected, we retry and wait 500 ms
  while (WiFiMulti.run() != WL_CONNECTED) {
    Serial.print(".");
  }
  //Display the Ip adress if it's connected.
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  // We Are Ready !!
}

void loop() {
  // put your main code here, to run repeatedly:
  read_sensors();
  send_data();
  get_data();
  delay(5000);
}
